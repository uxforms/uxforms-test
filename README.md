# test

A project for simplifying testing with UX Forms.

## Provides

### OsgiContainer

A base class for writing Pax Exam tests to ensure components can be deployed
successfully in to a UX Forms-compatible OSGi container.

Most likely to be used via the `test-plugin` sbt plugin.

## Releases

Keep the version number in sync with the version of `uxforms-dsl` it includes.