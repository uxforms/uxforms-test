package com.uxforms.test

import javax.inject.Inject

import org.ops4j.pax.exam.CoreOptions._
import org.ops4j.pax.exam.{Configuration, Option}
import org.osgi.framework.BundleContext

/**
  * Extend this class to write tests that verify your bundle's behaviour when deployed
  * to a UX Forms-compatible OSGi container.
  */
abstract class OsgiContainer {

  @Inject
  var context: BundleContext = _

  val scalaOptions = composite(
    mavenBundle("org.scala-lang", "scala-library", "2.11.7"),
    mavenBundle("org.scala-lang", "scala-reflect", "2.11.7"),
    mavenBundle("org.scala-lang", "scala-compiler", "2.11.7"),
    mavenBundle("org.scala-lang.modules", "scala-parser-combinators_2.11", "1.0.4"),
    mavenBundle("org.scala-lang.modules", "scala-xml_2.11", "1.0.4"),

    mavenBundle("joda-time", "joda-time", "2.8.1")
  )

  val testOptions = composite(
    junitBundles(),
    mavenBundle("com.uxforms", "test_2.11", "15.18.3")
  )

  val felixOptions = composite(
    bootDelegationPackages("javax.management", "org.w3c.dom", "sun.*", "com.sun.*", "javax.net.*"),
    cleanCaches()
  )

  val playJsonOptions = composite(
    mavenBundle("com.typesafe.play", "play-json_2.11", "2.4.3"),
    mavenBundle("com.typesafe.play", "play-iteratees_2.11", "2.4.3"),
    mavenBundle("org.scala-stm", "scala-stm_2.11", "0.8-ba104ce"),
    mavenBundle("com.typesafe.play", "play-functional_2.11", "2.4.3"),
    mavenBundle("com.typesafe.play", "play-datacommons_2.11", "2.4.3"),
    mavenBundle("com.chuusai", "shapeless_2.11", "2.1.0"),

    mavenBundle("com.fasterxml.jackson.core", "jackson-annotations", "2.5.4"),
    mavenBundle("com.fasterxml.jackson.core", "jackson-core", "2.5.4"),
    mavenBundle("com.fasterxml.jackson.core", "jackson-databind", "2.5.4"),

    mavenBundle("com.fasterxml.jackson.core", "jackson-annotations", "2.6.0"),
    mavenBundle("com.fasterxml.jackson.core", "jackson-core", "2.6.5"),
    mavenBundle("com.fasterxml.jackson.core", "jackson-databind", "2.6.5")
  )

  val loggingOptions = composite(
    mavenBundle("org.slf4j", "slf4j-api", "1.7.6").noStart(),
    mavenBundle("org.slf4j", "jul-to-slf4j", "1.7.6"),
    mavenBundle("ch.qos.logback", "logback-classic", "1.1.1"),
    mavenBundle("ch.qos.logback", "logback-core", "1.1.1")
  )

  val additionalMavenRepositoryURLs = Seq.empty[String]

  lazy val uxformsOptions = composite(
    systemProperty("org.ops4j.pax.url.mvn.repositories")
      .value("+https://artifacts-public.uxforms.net@id=uxforms-public" + additionalMavenRepositoryURLs.map(u => "," + u).mkString("")),

    mavenBundle("javax.servlet", "javax.servlet-api", "3.0.1"),
    mavenBundle("org.eclipse.jetty.orbit", "javax.servlet", "2.5.0.v201103041518"),
    mavenBundle("uk.co.bigbeeconsultants", "bee-client_2.11", "0.29.1"),
    mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.xmlresolver", "1.2_5"),
    mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.xerces", "2.11.0_1"),
    mavenBundle("commons-io", "commons-io", "2.4"),
    mavenBundle("commons-net", "commons-net", "3.5"),

    mavenBundle("com.ibm.icu", "icu4j", "67.1"),
    mavenBundle("com.uxforms", "i8n", "1.5.3"),

    mavenBundle("com.uxforms", "uxforms-dsl_2.11", "15.27.0"),
    mavenBundle("com.typesafe", "config", "1.3.0"),
    mavenBundle("com.vspy", "mustache_2.11", "1.3"),

    mavenBundle("com.uxforms", "uxforms-google-spreadsheets_2.11", "16.0.0"),
    mavenBundle("javax.mail", "mail", "1.4.7")
  )

  /**
    * Override this to have your project artifact installed into the OSGi container.
    * Use `mavenBundle("org","artifactId","version")` for known versions of project dependencies,
    * or `bundle("url")` to pick up an artifact from, say, your project's `/target` directory.
    *
    * `composite()` can be used to chain multiple dependencies together.
    */
  val projectArtifact: Option

  @Configuration
  def osgiContainer = options(
    scalaOptions,
    testOptions,
    felixOptions,
    playJsonOptions,
    loggingOptions,
    uxformsOptions,
    projectArtifact
  )


}
