package com.uxforms.test.container

import com.uxforms.test.OsgiContainer
import com.uxforms.test.bundle.BundleBuildInfo
import org.hamcrest.CoreMatchers._
import org.hamcrest.MatcherAssert._
import org.junit.Test
import org.junit.runner.RunWith
import org.ops4j.pax.exam.CoreOptions._
import org.ops4j.pax.exam.Option
import org.ops4j.pax.exam.junit.PaxExam
import org.ops4j.pax.exam.spi.reactors.{ExamReactorStrategy, PerSuite}
import org.ops4j.pax.exam.util.PathUtils

@RunWith(classOf[PaxExam])
@ExamReactorStrategy(Array(classOf[PerSuite]))
class ContainerStartupTest extends OsgiContainer {

  override val projectArtifact: Option = composite(
    systemProperty("logback.configurationFile").value(s"file:${PathUtils.getBaseDir}/src/test/resources/logback.xml"),
    bundle(BundleBuildInfo.artifact.toURI.toURL.toExternalForm)
  )

  @Test
  def contextIsInjectedIntoTests: Unit = {
    assertThat(context, notNullValue)
  }

}
