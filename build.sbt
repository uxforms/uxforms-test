
name := "test"

description := "A library of helper classes for testing UX Forms components"

lazy val commonSettings = Seq(
  organization := "com.uxforms",
  scalaVersion := "2.11.7"
)

val paxExamVersion = "4.6.0"


lazy val root = (project in file("."))
  .enablePlugins(BuildInfoPlugin, SbtOsgi)
  .settings(commonSettings)
  .settings(osgiSettings)
  .settings(libraryDependencies ++=
    Seq(
      "javax.inject" % "javax.inject" % "1",

      "org.ops4j.pax.exam" % "pax-exam-container-native" % paxExamVersion,
      "org.ops4j.pax.exam" % "pax-exam-junit4" % paxExamVersion,
      "org.ops4j.pax.exam" % "pax-exam-link-mvn" % paxExamVersion,
      "org.ops4j.pax.url" % "pax-url-aether" % "2.6.1",
      "org.apache.felix" % "org.apache.felix.framework" % "5.4.0",

      "com.novocode" % "junit-interface" % "0.11"
    )
  )
  .settings(
    publishTo := Some("uxforms-public" at "s3://artifacts-public.uxforms.net")
  )
  .settings(
    buildInfoObject := "RootBuildInfo",
    buildInfoPackage := "com.uxforms.test",
    buildInfoKeys ++= Seq[BuildInfoKey](
      "artifact" -> (Compile / packageBin / artifactPath).value
    )
  )
  .settings(
    OsgiKeys.exportPackage := Seq("com.uxforms.test.*"),
    (Test / test) := ((Test / test) dependsOn (OsgiKeys.bundle, bundleToTest / OsgiKeys.bundle)).value
  )
  .dependsOn(bundleToTest % "test")

lazy val bundleToTest = (project in file("bundle-to-test"))
  .enablePlugins(SbtOsgi, BuildInfoPlugin)
  .settings(commonSettings)
  .settings(osgiSettings)
  .settings(
    OsgiKeys.exportPackage := Seq("com.uxforms.test.bundle.*"),
    buildInfoKeys ++= Seq[BuildInfoKey](
      "artifact" -> (Compile / packageBin / artifactPath).value
    ),
    buildInfoPackage := "com.uxforms.test.bundle",
    buildInfoObject := "BundleBuildInfo"
  )

val osgiBundle = (ref: ProjectRef) => ReleaseStep(
  action = releaseStepTask(OsgiKeys.bundle).andThen(releaseStepTask(bundleToTest / OsgiKeys.bundle))
)

releaseProcess := (thisProjectRef apply { ref =>
  import ReleaseTransformations._
  Seq[ReleaseStep](
    checkSnapshotDependencies,
    inquireVersions,
    runClean,
    osgiBundle(ref), // insert this into the default release step order as runTest seems to ignore the dependsOn hooks below.
    runTest,
    setReleaseVersion,
    commitReleaseVersion,
    tagRelease,
    publishArtifacts,
    setNextVersion,
    commitNextVersion,
    pushChanges
  )
}).value
